-- Aventura básica para poder hacer otras más avanzadas con ella

import Data.List

type Lugar = String
type Cosa = String

data Identificador = Obj Cosa
                   | Puerta Lugar Lugar
		     deriving (Show, Eq)

type Mapa = [(Lugar, [Lugar])]


type Localizaciones = [(Cosa, Lugar)]
type Puertas = [(Identificador, [(Cosa, Lugar)])]
type Eventos = [(String, [(Cosa, Lugar)])]

type Mundo = (Localizaciones, Puertas, Eventos)

primero :: Mundo -> Localizaciones
primero (loc, _, _) = loc

segundo :: Mundo -> Puertas
segundo (_, puertas, _) = puertas

tercero :: Mundo -> Eventos
tercero (_, _, event) = event

-- PARTE MODIFICABLE
-- Aquí puedes modificar el mapa de la aventura
mapa :: Mapa
mapa = [
  ("entrada", ["pasillo"]),
  ("pasillo", ["entrada","sala de estudios", "biblioteca"]),
  ("sala de estudios", ["pasillo"]),
  ("biblioteca", ["pasillo", "almacén"]),
  ("almacén", ["biblioteca"])
  ]

-- Aquí puedes modificar el estado inicial del mundo
mundo :: Mundo
mundo = ([				-- Localización de objetos iniciales
    ("protagonista", "entrada"),
    ("libro", "biblioteca"),
    ("carnet", "sala de estudios"),
    ("orangután", "almacén"),
    ("tijeras", "almacén")
    ],
    [					-- "Puertas" que impiden hacer  cosas
    (Puerta "pasillo" "biblioteca", [("carnet", "protagonista")]),
    (Obj "libro", [("tijeras", "protagonista")]),
    (Obj "orangután", [("protagonista", "imposible")])
    ],
    [				        -- Eventos que ocurren
    ("pasillo",[]), 
    ("entrada", [("libro","protagonista")]),
    ("libro", []),
    ("almacén", []),
    ("tijeras", [])
    ]
    )

-- Algunos textos 
bienvenida = "\nBienvenido a esta aventura super tonta."

ayuda = "\nLos comandos que se pueden usar son:\n" ++
	"ir a lugar                -- para ir al lugar indicado\n" ++
	"coger cosa                -- para coger la cosa\n" ++
	"inventario                -- para ver los objetos que llevas\n" ++
	"ayuda                     -- para volver a ver esto\n" ++
	"salir                     -- para abandonar el juego."

despedida = "\n¡Hasta la vista!\n"

creditos = "\nJuego escrito por Benjamín."

-- EVENTOS
-- Texto que se muestra cuando un evento ocurre
texto_evento :: String -> String
texto_evento "pasillo" =
  "\n¿Estás preparado para una aventura sin igual?" ++
  "\n¿Serás capaz de superar mi reto?" ++
  "\nEncuentra el libro e intenta salir con vida de esta universidad." ++
  "\nMuaha cof cof... Perdón, tengo que mejorar mi risa maligna."

texto_evento "entrada" =
  "\nOh vaya, eso no fue demasiado complicado, tendré que" ++
  "\npensar en algo más difícil para la próxima vez" ++
  "\nMUAHA cof cof... ¡FUERA DE AQUÍ!"

texto_evento "libro" =
  "\n'Todo sobre la reproducción de los castores'..." ++
  "\nBueno, yo no soy nadie para juzgar las elecciones de cada cual," ++
  "\ndespués de todo, a mí me gustó Crepúsculo." ++
  "\n\nEs broma, en realidad aún no lo he terminado."

texto_evento "almacén" =
  "\n¡Cuidado un orangután! Me pregunto qué estará haciendo aquí." ++
  "\nNo parece peligroso, siempre que no intentes hacer daño a un" ++
  "\nlibro. Pero seamos realistas, ¿quién haría daño a un libro?" ++
  "\n" ++
  "\nEn serio, no destroces libros o un orangután te arrancará los" ++
  "\nbrazos y hará malavares con ellos. Está en su naturaleza."

texto_evento "tijeras" =
  "\nEspero que no estés pensando en cortar hojas con esas tijeras." ++
  "\nParece que el orangután no va a quitarte ojo a partir de ahora."

texto_evento _ = ""

-- Algunos eventos hacen cambios en el mundo:
evento_loc :: String -> Localizaciones -> Localizaciones
evento_loc "tijeras" loc =
   cambia ("orangután", "almacén") ("orangután", "siguiendo") loc
evento_loc _ loc = loc

-- PUERTAS
-- Texto que se muestra cuando una "puerta" bloquea el camino
texto_puerta :: Identificador -> String
texto_puerta (Puerta "pasillo" "biblioteca") =
  "\nPara entrar en esta exclusiva biblioteca necesitas un carnet."
texto_puerta (Obj "libro") =
  "\nVaya, parece que el libro está atado con una cuerda a la" ++
  "\nestantería. ¡Qué mala suerte! ¿No? Jijiji."
texto_puerta (Obj "orangután") =
  "\nNo creo que quieras hacer eso."
texto_puerta _ = ""


-- ACCIONES ESPECIALES
-- Cosas que no se pueden coger
es_cogible :: String ->  IO (Bool)
es_cogible "protagonista" = return False
es_cogible _ = return True

-- PARTE MENOS MODIFICABLE
-- PROGRAMA PRINCIPAL
-- IOs
main :: IO (String)
main = do
     putStrLn bienvenida
     putStrLn ayuda
     bucle_principal  (return mundo)
     putStrLn despedida
     getLine


bucle_principal :: IO (Mundo) -> IO ()
bucle_principal mundo =
  do mnd <- mundo
     if null (tercero mnd)
       then do putStrLn creditos
       	       return ()
       else do putStrLn $ intro_habitacion  mnd
               putStrLn "¿Qué vas a hacer?\n"
               respuesta <- getLine
	       putStrLn ">>>>>>>>>>>>>>>"
               if respuesta == "salir"
                  then return ()
                  else do bucle_principal $ accion respuesta mnd

-- ACCIONES
accion :: String -> Mundo -> IO (Mundo)
accion "ayuda" mundo = 
  do putStrLn ayuda
     return mundo

accion ('i':'r':' ':'a':' ':resto) mundo =
  do (puede, new_puertas) <- puede_ir resto mundo  
     if puede
       then do (new_loc, nn_puertas, neu_eventos) <- 
                  evento resto (loc, new_puertas, lista_even)
       	       return (ir_a resto new_loc, nn_puertas, neu_eventos)
       else do putStrLn "\nNo puedes ir allí.\n"
               return mundo
  where loc = primero mundo
        puertas = segundo mundo
	lista_even = tercero mundo

accion ('c':'o':'g':'e':'r':' ':resto) mundo =
  do (puede, new_puertas) <- puede_coger resto mundo
     if puede
       then do putStrLn $ "\nHas cogido " ++ resto
       	       (new_loc, nn_puertas, neu_eventos) <- 
                      evento resto (loc, new_puertas, eventos)
               return (coger resto new_loc, nn_puertas, neu_eventos)
       else do putStrLn $ "\nNo puedes coger eso."
               return mundo
  where loc = primero mundo
        eventos = tercero mundo

accion "inventario" mundo = 
  do putStrLn "\nTienes los siguientes objetos:"
     putStrLn $ pon_objetos "protagonista" loc
     return mundo
  where loc = primero mundo

accion _ mundo =
  do putStrLn "No te entiendo\n"
     return mundo

-- IR A
puede_ir :: String -> Mundo -> IO (Bool, Puertas)
-- puede_ir _ _ = return (False,[])   --stub
puede_ir sitio mundo = 
  do (no_hay, new_puertas) <- no_hay_puerta sitio mundo
     return (((elem sitio (encuentra_habitacion habitacion mapa)) && no_hay),
             new_puertas)
  where loc = primero mundo
        habitacion = encuentra "protagonista" loc

-- COGER
puede_coger :: String -> Mundo -> IO (Bool, Puertas)
-- puede_coger _ _ = return (False, []) -- stub
puede_coger cosa (loc, puertas, eventos) =
  do cogible <- es_cogible cosa 
     if esta_cerca && cogible
        then do (no_hay, new_puertas) <- 
                   no_hay_puerta cosa (loc, puertas, eventos)
                return (no_hay, new_puertas)
        else return (False, puertas)
  where habitacion = encuentra "protagonista" loc
        esta_cerca = 
           (elem (cosa, habitacion) loc) || (elem (cosa, "siguiendo") loc)

-- EVENTOS
evento :: String -> Mundo -> IO (Mundo)
-- evento _ _ = return ([],[],[]) -- stub
evento _ (loc , puerta, [])  = return (loc, puerta, [])
evento str (loc, puertas, even:eventos) =
  if (str == fst even) && (todos cond loc)
     then do putStrLn $ texto_evento str
     	     return (evento_loc str loc, puertas, eventos)
     else do (n_loc, n_puertas,rec) <- evento str (loc, puertas, eventos)
             return (n_loc, n_puertas, even:rec)
  where cond = snd even

-- PUERTAS
no_hay_puerta :: String -> Mundo -> IO (Bool, Puertas)
-- no_hay_puerta _ _ = return (False, [])  -- stub
no_hay_puerta _ (_, [],_) = return (True, [])
no_hay_puerta str (loc, puert:puertas, eventos) =
  if (fst puert == Puerta habitacion str) || (fst puert == Obj str)
    then if (todos cond loc)
            then return (True, puertas)
            else do putStrLn $ texto_puerta (fst puert)
                    return (False, puert:puertas)
    else do (bol, rec) <- no_hay_puerta str (loc, puertas, eventos)
            return (bol, puert:rec)
  where habitacion = encuentra "protagonista" loc
        cond = snd puert


-- PARTE FUCIONAL
-- Fuciones que dan un texto bonito:
intro_habitacion :: Mundo -> String
intro_habitacion mundo =
  "\nEstas en " ++
  habitacion ++ "\n" ++
  "Puedes ver los siguientes objetos:\n" ++
  (pon_objetos habitacion localizaciones) ++ 
  (pon_objetos "siguiendo" localizaciones) ++
  "\nPuedes ir a los siguientes sitios:\n " ++
  (pon_sitios habitacion)
  where localizaciones = primero mundo
        habitacion = encuentra "protagonista" localizaciones

pon_objetos :: String -> Localizaciones -> String
pon_objetos cadena mundo =
  pon_linea cadena ++
  (intercalate "\n " (objetos cadena mundo))
  where objetos _ [] = []
  	objetos str (x:xs) =
          if (str == (snd x)) && ((fst x) /= "protagonista")
	    then (fst x):(objetos str xs)
	    else (objetos str xs)
        pon_linea "siguiendo" = 
          if null (objetos "siguiendo" mundo)
             then " "
             else "\n "
        pon_linea _ = " "

pon_sitios :: String -> String
pon_sitios habitacion = 
  intercalate "\n " (encuentra_habitacion habitacion mapa)


-- ACCIONES

ir_a :: String -> Localizaciones -> Localizaciones
ir_a _ [] = []
ir_a sitio (("protagonista",_):masmundo) = ("protagonista",sitio):masmundo
ir_a sitio (cosa:masmundo) = cosa:(ir_a sitio masmundo)
        
coger :: String -> Localizaciones -> Localizaciones
coger cosa [] = []
coger cosa (otracosa:masmundo) = 
  if (fst otracosa) == cosa
    then (cosa, "protagonista"):masmundo
    else otracosa:(coger cosa masmundo)

-- UTILIDADES
-- Ayudas al programa a situarse en el mundo
encuentra_habitacion :: String -> Mapa -> [String]
encuentra_habitacion _ [] = []
encuentra_habitacion habitacion (x:xs) =
   if habitacion == fst x
     then snd x
     else encuentra_habitacion habitacion xs

encuentra :: String -> Localizaciones -> String
encuentra _ [] = ""
encuentra str (x:xs) =
  if str == fst x
    then snd x
    else encuentra str xs


-- Cosas genéricas
todos :: Eq a => [a] -> [a] -> Bool
-- todos _ _ = False		-- stub
todos [] _ = True
todos (x:xs) lista =
  elem x lista && todos xs lista

cambia :: Eq a => a -> a -> [a] -> [a]
-- cambia _ _ xs = xs		-- stub
cambia _ _ [] = []
cambia x y (z:zs) =
  if x == z
    then y:zs
    else z:(cambia x y zs)
